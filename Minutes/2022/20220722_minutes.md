# Notes from meeting 22 07 2022

## Present
Malte, Gerrit, Oonagh (minutes)

## Updates

- Gerrit has worked extensively on metadata for the high-level landing page
  - two parts: xml filled with detailed metadata; html which extracts information from xml
  - follow [DataCite](https://schema.datacite.org/meta/kernel-4.4/) standard
  - subtlety/difficulty in ensuring links to lower-level landing pages are clearly specified/defined for machines/humans
  - first html rendering of landing page mock-ups
  - sorting of information between high-level and lower-level pages

**to do** 
- Vivien to work on html code to improve presentation
- how to present root files so they can be reused
  - discuss this with root team 
- apply standards for units in data files
- contact RUCIO people

**questions**
- how long is customised class code for root file
  - Malte took a look approx 450 lines
  - can we have a copy of this code? potentially work on refactoring it

**communication**
- poster at [DMA meeting](https://indico.desy.de/event/33132/) in September? (Gerrit will attend this)
- short presentation at [HMC conference](https://events.hifis.net/event/469/)
- concept paper? ideas?
  - integrate in data type registery?
