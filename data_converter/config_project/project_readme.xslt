<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8"/>

  <xsl:template match="/">
    <html>
      <head>
        <title>README</title>
        <style>
          table{
            background-color: #d0d0d0;
            border: 0;
            border-radius: 10px;
            padding: 15px;
            margin-top: 10px;
            margin-bottom: 20px;
            margin-left: 10px;
            margin-right: 10px;
            vertical-align: top;
            float: left;
            border-top: 7px solid #becd00;
          }
          .box{
            position: relative;
            display: inline-block; /* Make the width of box same as image */
          }
          .box .text{
            position: absolute;
            z-index: 999;
            margin: 0 auto;
            left: 0;
            right: 0;
            top: 0%;
            text-align: right;
            width: 90%;
          }
          div.author_box {
            margin-top: -10px;
            margin-bottom: 25px;
            margin-left: 10px;
            margin-right: 10px;
          }
          td, th {
            text-align: left;
            vertical-align: top;
            padding: 5px 20px 0px 0px;
          }
          table.nobox{
            background-color: transparent;
            border: 0;
            padding: 0px;
            margin-top: 0px;
            margin-bottom: 0px;
            margin-left: 0px;
            margin-right: 0px;
            vertical-align: top;
            float: left;
          }
        </style>
      </head>

      <body>

        <!-- PART 1: title, authors... -->
        <!-- TITLE -->
        <xsl:for-each select="*/titles">
          <h2 style="margin-left:30px;margin-top:30px"><xsl:value-of select="title"/></h2>
        </xsl:for-each>

        <!-- AUTHORS -->
        <div class="author_box" style="margin-left:30px">
          <tr>
            <td>
              <xsl:for-each select="*/creators/creator">
                <xsl:value-of select="givenName"/>&#160;<xsl:value-of select="familyName"/> <a href="{concat('https://orcid.org/', nameIdentifier)}"><img alt="ORCID logo" src="https://info.orcid.org/wp-content/uploads/2019/11/orcid_16x16.png" width="16" height="16" /></a>
                <xsl:if test="position() != last()">
                  <xsl:text>, </xsl:text>
                </xsl:if>
              </xsl:for-each>
            </td>
          </tr>
        </div>

        <!-- PART 2: abstract, instrument... -->
        <table class="nobox" style="margin-left:0px;margin-top:-20px;width:90%;padding:0px">
            <!-- FIRST COLUMN -->
            <td style="width:50%">
                <!-- ABSTRACT -->
                <table style="padding-left:0px;width:100%">
                  <tr>
                    <td>
                      <td><h3>Abstract</h3>&#xa;<xsl:value-of select="*/abstract"/></td>
                    </td>
                  </tr>
                </table>

                <!-- INCLUDED DATA SETS -->
                <table style="width:100%">
                  <tr>
                    <td number-columns-spanned="2">
                      <h3>Included Data Sets</h3>
                    </td>
                  </tr>
                  <tr>
                    <th style="text-align:left">File</th>
                    <th style="text-align:left">Description</th>
                  </tr>
                  <xsl:for-each select="*/datasets/a">
                    <xsl:choose>
                      <xsl:when test="not(contains(text(), 'report_data_converter.html'))">
                        <tr>
                          <td>
                            <a href="{@href}"><xsl:value-of select="text()"/></a>
                          </td>
                          <td>
                            <xsl:choose>
                              <xsl:when test="@description != ''">
                                <xsl:value-of select="@description"/>
                              </xsl:when>
                              <xsl:otherwise>
                                <em> No description available </em>
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                        </tr>
                      </xsl:when>
                    </xsl:choose>
                  </xsl:for-each>
                </table>

                <!-- SOFTWARE DECLARATION -->
                <table class="nobox" style="width:100%;margin-left:25px">
                  <xsl:for-each select="*/datasets/a">
                    <xsl:choose>
                      <xsl:when test="(contains(text(), 'report_data_converter.html'))">
                        <tr>
                          <td>
                            Note: Files of the data set were processed with the data converter producing the log file:
                            <a href="{@href}"><xsl:value-of select="text()"/></a>.
                          </td>
                        </tr>
                      </xsl:when>
                    </xsl:choose>
                  </xsl:for-each>
                </table>
            </td>

            <!-- SECOND COLUMN -->
            <td style="width:40%">
                <!-- Instrument Image + PID -->
                <table class="nobox" style="width:100%">
                  <tr>
                    <td>
                      <div class="box">
                        <img alt="A4 experiment" src="../../data_converter/config_project/A4_instrument_schema.png" width="100%" />
                        <div class="text">
                          <xsl:for-each select="*/relatedItems/relatedItem">
                            <xsl:choose>
                              <xsl:when test="@relatedItemType='Instrument persistent identifier'">
                                <figcaption>Instrument PID: <a href="http://doi.org/{relatedItemIdentifier}">
                                                              <xsl:value-of select="relatedItemIdentifier"/>
                                                           </a>
                                </figcaption>
                              </xsl:when>
                            </xsl:choose>
                          </xsl:for-each>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>

                <!-- REFERENCES -->
                <table style="width:100%">
                  <tr>
                    <td>
                      <h3>References</h3>
                    </td>
                  </tr>
                  <xsl:for-each select="*/relatedItems/relatedItem">
                    <xsl:choose>
                      <xsl:when test="@relatedItemType!='Instrument persistent identifier'">
                        <tr>
                          <td>
                            -&#xa;<a href="http://doi.org/{relatedItemIdentifier}"><xsl:value-of select="titles/title"/>, &#xa;<xsl:value-of select="citation"/></a>
                          </td>
                          <td>
                            <xsl:value-of select="@description"/>
                          </td>
                        </tr>
                      </xsl:when>
                    </xsl:choose>
                  </xsl:for-each>
                </table>

            </td>
        </table>

      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
