<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8"/>

  <xsl:template match="/">
    <html>
      <head>
        <title>Report Log</title>
        <style>
          table{
            background-color: #d0d0d0;
            border: 0;
            border-radius: 10px;
            padding: 15px;
            margin-top: 10px;
            margin-bottom: 20px;
            margin-left: 10px;
            margin-right: 10px;
            vertical-align: top;
            float: left;
            border-top: 7px solid #becd00;
          }
          .box{
            position: relative;
            display: inline-block; /* Make the width of box same as image */
          }
          .box .text{
            position: absolute;
            z-index: 999;
            margin: 0 auto;
            left: 0;
            right: 0;
            top: 0%;
            text-align: right;
            width: 90%;
          }
          div.author_box {
            margin-top: -10px;
            margin-bottom: 25px;
            margin-left: 10px;
            margin-right: 10px;
          }
          td, th {
            text-align: left;
            vertical-align: top;
            padding: 5px 20px 0px 0px;
          }
        </style>
      </head>

      <body>

        <!-- TITLE -->
        <h2 style="margin-left:30px;margin-top:30px">Data Converter Report Log</h2>

        <!-- MISSING RDF DEFINITIONS -->
        <table style="width: 95%">
          <tr>
            <td number-columns-spanned="2">
              <h3>Missing rdf Definitions</h3>
            </td>
          </tr>
          <tr>
            <th style="text-align:left;width:40%">Term</th>
            <th style="text-align:left">Log Message</th>
          </tr>
          <xsl:for-each select="*/missingDefinitions/missingDefinition">
            <tr>
              <td>
                <xsl:value-of select="resource"/>
              </td>
              <td>
                <xsl:value-of select="message"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>

        <!-- MERGED LINES -->
        <table style="width: 95%">
          <tr>
            <td number-columns-spanned="2">
              <h3>Merged Lines</h3>
            </td>
          </tr>
          <tr>
            <th style="text-align:left;width:40%">File</th>
            <th style="text-align:left">Log Message</th>
          </tr>
          <xsl:for-each select="*/mergedLines/mergedLine">
            <tr>
              <td>
                <xsl:value-of select="file"/>
              </td>
              <td>
                <xsl:value-of select="message"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>

      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
