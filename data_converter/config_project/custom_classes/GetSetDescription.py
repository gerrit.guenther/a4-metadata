# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


import os
import re
import sys
from pathlib import Path

path = Path(__file__)
sys.path.append(str(path.parent.absolute()))

from auxiliary.general_aux import *


class GetSetDescription(object):
    """
    Collects data and returns it.
    """

    def __init__(self, _current_dir, _readme_dict={}, _sum_dict={}):
        """
        Initializes class variables.
        """
        self.current_dir = _current_dir
        self.description = ""

        pass

    def start(self):
        """
        Routine that assigns data to .
        :param _path: String containing the path to a json file,
        e.g. "D:/a4-metadata/data_converter/config_project/include_data.json".
        :return: String (containing the description of the data set to be written to the top-level readme file).
        """

        log_path = self.upward_search(self.current_dir)

        if not log_path == "":
            log_string = file2string(log_path, "elog_run_entry.log")

            run_time = self.word_after("Runtime =", log_string)
            foerster = self.ending_after("Foerster=", log_string)

            _delimiter = ", "
            self.description = "Measurement with Foerster: " + foerster + _delimiter + \
                "run time: " + run_time

            # self.value = self.value.replace("\n", "&#xD;")

        return self.description

    @staticmethod
    def upward_search(_path):
        # searching for file "elog_run_entry.log" by going through folders upwards (and search all child folders)
        searching = True
        _current_path = _path
        log_path = ""
        while searching:
            if os.path.sep in _current_path:
                for root, dirs, files in os.walk(_current_path):
                    for name in files:
                        if name == "elog_run_entry.log":
                            log_path = root
                            searching = False
                            break
                    if not searching:
                        break
                # going 1 level up
                _current_path = _current_path.rsplit(os.path.sep, 1)[0]
            else:
                searching = False

        return log_path

    def ending_after(self, _keyword, _string):
        """
        Returns the part of a word (i.e. string confined by whitespaces) following the keyword; e.g. keyword 'Foerster='
        returns '18015' of string 'example Foerster=18015 followed by some words'.
        :param _keyword: String containing the preceding part of wanted expression within the same word,
        e.g. "Foerster=".
        :param _string: String containing the text string which contains the keyword,
        e.g. 'example Foerster=18015 followed by some words'.
        :return: String (containing the part that follows _keyword).
        """
        search_string = re.compile(_keyword + r"\w+")
        return re.search(search_string, _string)[0].replace(_keyword, "")

    def word_after(self, _keyword, _string):
        search_string = re.compile(_keyword + r"(\s+)\w+")
        return re.search(search_string, _string)[0].split(_keyword)[1]
