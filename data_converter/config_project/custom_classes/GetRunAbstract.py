# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


import re
import sys
from pathlib import Path

path = Path(__file__)
sys.path.append(str(path.parent.absolute()))

from auxiliary.general_aux import *


class GetRunAbstract(object):
    """
    Collects data and returns it.
    """

    def __init__(self, _current_dir, _readme_dict={}, _sum_dict={}):
        """
        Initializes class variables.
        """
        self.current_dir = _current_dir
        self.value = ''

        pass

    def start(self):
        """
        Routine that assigns data to .
        :param _path: String containing the path to a json file,
        e.g. "D:/a4-metadata/data_converter/config_project/include_data.json".
        :return: Any type (depends on the type assigned to self.value).
        """
        log_string = file2string(self.current_dir, "elog_run_entry.log")

        subject = self.word_after("Subject:", log_string)
        type = self.word_after("Type:", log_string)
        run_purpose = self.word_after("Runpurpose:", log_string)
        run_comment = self.word_after("Runcomment:", log_string)

        run_time = self.word_after("Runtime =", log_string)
        foerster = self.ending_after("Foerster=", log_string)

        _delimiter = ", "
        self.value = "Subject: " + subject + _delimiter +\
            "Type: " + type + _delimiter +\
            "Run purpose: " + run_purpose + _delimiter +\
            "Run comment: " + run_comment + _delimiter +\
            "Run time: " + run_time + _delimiter +\
            "Foerster: " + foerster

        # self.value = self.value.replace("\n", "&#xD;")

        return self.value

    def ending_after(self, _keyword, _string):
        """
        Returns the part of a word (i.e. string confined by whitespaces) following the keyword; e.g. keyword 'Foerster='
        returns '18015' of string 'example Foerster=18015 followed by some words'.
        :param _keyword: String containing the preceding part of wanted expression within the same word,
        e.g. "Foerster=".
        :param _string: String containing the text string which contains the keyword,
        e.g. 'example Foerster=18015 followed by some words'.
        :return: String (containing the part that follows _keyword).
        """
        search_string = re.compile(_keyword + r"\w+")
        return re.search(search_string, _string)[0].replace(_keyword, "")

    def word_after(self, _keyword, _string):
        search_string = re.compile(_keyword + r"(\s+)\w+")
        return re.search(search_string, _string)[0].split(_keyword)[1]
